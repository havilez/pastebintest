import { HttpModule } from '@angular/http';
import { TestBed, inject } from '@angular/core/testing';

import { AppModule } from './app.module';
import { PastebinService } from './pastebin.service';
import { Pastebin, Languages } from './pastebin';

let testService: PastebinService;
let mockPaste: Pastebin, mockPaste2: Pastebin;
let responsePropertyNames, expectedPropertyNames;



describe('PastebinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule],
      providers: [ PastebinService]
    });

    testService = TestBed.get(PastebinService);
    mockPaste = { id: 999, title: 'Hello world', language: Languages[2], paste: "console.log('Hello world');"};
    mockPaste2 = { id: 1, title: 'A new title', language: Languages[2], paste: "console.log('Hello world');"};

  });

  it('should be created', inject([PastebinService], (service: PastebinService) => {
    expect(service).toBeTruthy();
  }));

  it('#getPastebin should return an array with Pastebin objects', async() => {
    testService.getPastebin().then( value => {
      // Checking the property names of the returned object and mockPaste
      responsePropertyNames = Object.getOwnPropertyNames( value[0] );
      expectedPropertyNames = Object.getOwnPropertyNames( mockPaste);

      expect(responsePropertyNames).toEqual(expectedPropertyNames);
    });
  });

  it('#addPaste should return async paste', async() => {
    testService.addPaste(mockPaste).then(value => {
      expect(value).toEqual(mockPaste);
    });
  });

  it('#updatePaste should update', async() => {
    // updating the title of Paste with id 1
    mockPaste.id = 1;
    mockPaste.title = 'New title';

    testService.updatePaste(mockPaste).then(value => {
      expect(value).toEqual(mockPaste);
    });
  });

  it('#deletePaste should return null', async() => {
    testService.deletePaste(mockPaste).then( value => {
      expect(value).toEqual(null);
    })
  })

});
