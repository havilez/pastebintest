import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { PastebinService } from '../pastebin.service';
import { AddPasteComponent } from './add-paste.component';



describe('AddPasteComponent', () => {
  let component: AddPasteComponent;
  let fixture: ComponentFixture<AddPasteComponent>;
  let de: DebugElement;
  let element: HTMLElement;
  let spy: jasmine.Spy;
  let pastebinService: PastebinService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPasteComponent ],
      imports: [ HttpModule, FormsModule ],
      providers: [ PastebinService ]
    })
    .compileComponents();

  }));

 beforeEach(() => {
   // initialization
   fixture = TestBed.createComponent( AddPasteComponent );
   pastebinService = fixture.debugElement.injector.get( PastebinService );
   component = fixture.componentInstance;
   de = fixture.debugElement.query( By.css('.add-paste'));
   element = de.nativeElement;
   spy = spyOn(pastebinService, 'addPaste').and.callThrough();

   // ask fixture to detect changes
   fixture.detectChanges();
 });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display the `create Paste` button', () => {
    // There should be a create button in the template
    expect(element.innerText).toContain('create Paste');
  });

  it('should not display the modal unless the button is clicked', () => {
    // source-model is an id for the modal. It shouldn't show up unless
    // create button is clicked
    expect( element.innerHTML).not.toContain('source-modal');

    // Component's showModal property should be false at the moment
    expect(component.showModal).toBeFalsy('Show modal should be initialized to false');
  });

  it('should display the modal when `create Paste` is clicked', () => {
    const createPasteButton = fixture.debugElement.query(By.css('button'));

    // create a spy on createPaste method
    spyOn( component, 'createPaste').and.callThrough();

    // trigger EventHandler simulates a click event on the button object
    createPasteButton.triggerEventHandler('click', null);

    // spy checks whether the method was called
    expect(component.createPaste).toHaveBeenCalled();
    fixture.detectChanges();

    expect(component.showModal).toBeTruthy('showModal should be true');
    expect(element.innerHTML).toContain('source-modal');

  });

});
