import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { PastebinComponent } from './pastebin.component';
import { By } from '@angular/platform-browser';
import { Pastebin, Languages } from '../pastebin';
import { PastebinService } from '../pastebin.service';

// Modules used for testing
import { HttpModule } from '@angular/http';

describe('PastebinComponent', () => {

// Typescript declarations.
  let comp: PastebinComponent;
  let fixture: ComponentFixture<PastebinComponent>;
  let de: DebugElement; // DOM on the component
  let element: HTMLElement;
  let mockPaste: Pastebin[];
  let spy: jasmine.Spy;

  // beforeEach is called once before every `it` block in a test.
  // Use this to configure to the component, inject services etc.

  beforeEach(async( () => { // async before is used for compiling external templates which is any async activity

    // The TestBed is responsible for configuring and initializing the environment that we are going to write our tests in.
    // By calling TestBed.configureTestingModule, we can set up a special testing module that allows us to test our component.
    // This is very much like defining an ngModule in that we can define declarations, imports, providers, etc
    // that we want to expose to our component
    TestBed.configureTestingModule({
        declarations: [ PastebinComponent/*, PastebinService*/ ], // declare the test component
        providers: [ PastebinService ],
        imports: [ HttpModule],
    })
    .compileComponents(); // compile template and css
  }));

 beforeEach( () => {
    // ComponentFixture comes into play as it gives us a reference
    // to the component instance being tested, as well as the component’s template.
    fixture = TestBed.createComponent(PastebinComponent);

   // With our component fixture initialized, we can get a reference to the component we are testing by calling fixture.componentInstance,
   //  which we will assign to component.
    comp = fixture.componentInstance;

    // get DOM on the component
    de = fixture.debugElement;
    // .query(By.css('.button-text'));
    // de = fixture.debugElement.query(By.css('div'));
     element  = de.nativeElement;

     // The real PastebinService is injected into the component
     let  pastebinService = fixture.debugElement.injector.get(PastebinService);
    mockPaste = [
      { id: 1, title: 'Hello world', language: 'Ruby', paste: "puts 'Hello'" }];

    spy = spyOn(pastebinService, 'getPastebin')
      .and.returnValue(Promise.resolve(mockPaste));


 });

 it('should have a Component', () => {
      expect(comp).toBeTruthy();
  });

  it('should have a title', () => {
    comp.title = 'Pastebin Application';
    // upon changing component field, force render update
    fixture.detectChanges();

    expect(element.textContent).toContain(comp.title);
  });

  it('should have a table to display the pastes', () => {
    expect(element.innerHTML).toContain('thead');
    expect(element.innerHTML).toContain('tbody');
  });

  it('should not show the pastebin before OnInit', () => {
    this.tbody = element.querySelector('tbody');

   // Try this without the 'replace(\s\s+/g,'')' method and see what happens
   expect(this.tbody.innerText.replace(/\s\s+/g, '')).toBe('', 'tbody should be empty');
    expect( spy.calls.any()).toBe(false, "Spy shouldn't be yet called");
  });

  it('should still not show pastebin after component initialized', () => {
    fixture.detectChanges();
   // getPastebin service is async, but the test is not.
    expect(this.tbody.innerText.replace(/\s\s+/g, '')).toBe('', 'tbody should still be empty');
    expect(spy.calls.any()).toBe(true, 'getPastebin should be called');
  });

});
